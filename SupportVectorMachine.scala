import org.apache.spark.mllib.classification.{SVMModel, SVMWithSGD}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}


object SupportVectorMachine extends App {

  def getSparkContext: SparkContext = {
    //get spark context
    //create spark context config
    val conf = new SparkConf().
      setMaster("local").
      setAppName("DrillOptimizer").
      setSparkHome("SPARK_HOME").
      set("spark.executor.memory", "512m").
      set("spark.cleaner.ttl", "3600")
    new SparkContext(conf)

  }

  val sc = getSparkContext

  val wellData = sc.textFile(args(0)) //read in supplied file

  val data = wellData.map(_.split(" ").map(_.toDouble)).cache()

  val trainingData = data.map { row =>
    LabeledPoint(row.head, Vectors.dense(row.tail))

  }
  //val scalar = new StandardScaler(withMean = true, withStd = true).fit(trainingData.map(x => x.features))
  //val scaledTraining = trainingData.map(x => LabeledPoint(x.label, scalar.transform(Vectors.dense(x.features.toArray))))
  //scaledTraining.foreach(println(_))
  var preds = List[(Double, Double)]()
  val avgErr = runSimulation(trainingData)
  sc.stop()
  println(s"Score And Labels: $preds")

  //println(s"mean area under curve:  $avgErr")

  def runSimulation(trainingSet: RDD[LabeledPoint]) = {
    val loops = trainingSet.count().toInt
    //println(s"loops: $loops")
    val allData = {
      // filter observation in the current iteration from the training set
      for {count <- 0 until loops} yield
        (trainingSet.collect().apply(count), trainingSet.collect().filterNot(x => x == trainingSet.collect().apply(count)))
    }
    val testData = allData.map { data =>
      data._1
    }.toSeq
    //println(s"test data: ${testData.toVector}")
    val trainData = allData.map { data =>
      data._2.map { d => d}
    }.toSeq.map(sc.parallelize(_))
    // println(s"train data: ${trainData.toVector}")

    for (count <- 0 until trainData.size) {
      val model = produceModel(trainData(count))
      predScoreAndLabels(model, testData(count))

    }

    val auc = calculateAreaUnderCurve(sc.makeRDD(preds))
    println(s"Area Under Curve: $auc")


  }

  def produceModel(training: RDD[LabeledPoint]) = {
    // Run training algorithm to build the model
    val numIterations = 10
    val model = SVMWithSGD.train(training, numIterations, 1e-4, 0.1)

    // Clear the default threshold.
    //println(s"weights: ${model.weights}")
    model
  }

  def predScoreAndLabels(model: SVMModel, test: LabeledPoint) = {
    model.setThreshold(0.5)
    //model.clearThreshold()
    // Compute raw scores on the test set.
    val score = model.predict(test.features)
    //val readableScore = s"score: $score, label: ${test.label}, features: ${test.features}\n"
    preds ::=(score, test.label)

  }

  def calculateAreaUnderCurve(scoreAndLabels: RDD[(Double, Double)]) = {
    // Get evaluation metrics.
    val metrics = new BinaryClassificationMetrics(scoreAndLabels)
    //return auROC
    val auROC = metrics.areaUnderROC()
    //println(s"ROC: ${metrics.roc().collect().toVector}")
    auROC
  }


  /*def produceModel(trainingSet: RDD[LabeledPoint]): LogisticRegressionModel = {
    val trainingTuple = trainingSet.map(x => (x.label, MLUtils.appendBias(x.features)))
    val numFeatures = trainingTuple.first()._2.size
    val numCorrections = 10
    val convergenceTol = 1e-8
    val maxNumIterations = 20
    val regParam = 0.1
    val initialWeightsWithIntercept = Vectors.dense(new Array[Double](numFeatures))

trainingTuple.collect().foreach(x=> println(s"label: ${x._1}, features: ${x._2}"))
    val (weightsWithIntercept, loss) = LBFGS.runLBFGS(
      trainingTuple,
      new LogisticGradient(),
      new SquaredL2Updater(),
      numCorrections,
      convergenceTol,
      maxNumIterations,
      regParam,
      initialWeightsWithIntercept)

    val model = new LogisticRegressionModel(
      Vectors.dense(weightsWithIntercept.toArray.slice(0, weightsWithIntercept.size - 1)),
      weightsWithIntercept(weightsWithIntercept.size - 1)).clearThreshold()
model
  }*/
}

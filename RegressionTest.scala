import org.apache.spark.mllib.classification.LogisticRegressionModel
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.rdd.RDD

import scala.math._

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.optimization._
import org.apache.spark.mllib.regression._
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.regression.RidgeRegressionModel


object RegressionTest extends App {

  val sc = getSparkContext
  val wellData = sc.textFile(args(1)) //read in supplied file
  val wellData2 = sc.textFile(args(0)) //read in supplied file

  val data = wellData.map(_.split(" ").map(_.toDouble)).cache()
  val data2 = wellData2.map(_.split(" ").map(_.toDouble)).cache()
  //read one line at a time tokenizing on spaces
  val trainingSet = data.map {
    row => //read one array at a time
      val transformedData = row//BourgoyneYoungModel(row) //transform features according to Bourgoyne and Young 1974 drilling model
      // val scalar = new StandardScaler(withMean = true, withStd = true).fit(arrayToRDDVector(Array(transformedData)))


      // LabeledPoint(transformedData.head, Vectors.dense(transformedData.tail)) //prepare predictor and output for regression
      (transformedData.head, MLUtils.appendBias(Vectors.dense(transformedData.tail)))
  }
  trainingSet.foreach(println(_))

  /* val model = produceModel(trainingSet)
   println("weights---------------------------")
   println(model.weights)
   println("MSE--------------------------------")
   val mse = calculateMeanSquaredError(model, trainingSet.first())
   println(mse)*/

  def getSparkContext: SparkContext = {
    //get spark context
    //create spark context config
    val conf = new SparkConf().
      setMaster("local").
      setAppName("DrillOptimizer").
      setSparkHome("SPARK_HOME").
      set("spark.executor.memory", "512m").
      set("spark.cleaner.ttl", "3600")
    new SparkContext(conf)

  }

  def BourgoyneYoungModel(params: Array[Double]): Array[Double] = {
    import scala.math.log
    val y = log(params(0))
    val x2 = 10000 - params(1)
    val x3 = pow(params(1), 0.69) * 1//(params(8) - 9)
    val x4 = params(1) * 1//(params(8) - params(7))
    val x5 = log((params(3) - 0.02) / (4 - 0.02))
    val x6 = log(params(4) / 60)
    val x7 = params(5) * (-1)
    //val x8 = params(6)
    //Array(y, x2, x3, x4, x5, x6, x7, x8)
    Array(y, x2, x3, x4, x5, x6, x7)
    //param(2) is not used bit number
  }

  def calculateROP(model: GeneralizedLinearModel, data: RDD[Array[Double]]) = {
    println(data.first().toVector)
    println(model.weights+","+model.intercept)
    import scala.math.log
    //drilling constants from multiple regression after Bourgoyne and Young 1974
    val constants = model.weights.toArray // transform weight vector to array
    val a1 = model.intercept
    val a2 = constants(0)
    val a3 = constants(1)
    val a4 = constants(2)
    val a5 = constants(3)
    val a6 = constants(4)
    val a7 = constants(5)
//    val a8 = constants(6)

    val rop = data.map {
      row =>
        val f1 = exp(a1)
        val f2 = exp(a2 * (8000 - row(1)))
        val f3 = exp(a3 * pow(row(1), 0.69) * (row(8) - 9))
        val f4 = exp(a4 * (row(1) * (row(8) - row(7))))
        val f5 = exp(a5 * log((row(3) - 0.02) / (4 - 0.02)))
        val f6 = exp(a6 * log(row(4) / 60))
        val f7 = exp(a7 * (row(5) * (-1)))
        //val f8 = exp(a8 * row(6))
        /* val _rop = a1 + (a2 * (8000 - row(1))) //depth
         +(a3 * (pow(row(1), 0.69) * (row(8) - 9))) //compaction
         +(a4 * (row(1) * (row(8) - row(7)))) //pressure
         +a5 * log((row(3) - 0.02) / (4 - 0.02)) //WOB
         +a6 * log(row(4) / 60) //RPM
         +a7 * (row(5) * (-1)) //bit wear
         +a8 * row(6) //hydraulics
         exp(_rop)*/
        //f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8
        f1 * f2 * f3 * f4 * f5 * f6 * f7
    }
    //rop.saveAsTextFile("file:///c:/tools/data/rop_lbfgs_PCA_proj") // write file for plotting
    rop
  }

  private def arrayToRDDVector(array: Array[Array[Double]]) = {
    sc.parallelize(array.map(values => Vectors.dense(values.tail)))
  }

  /* def produceModel(trainingData: RDD[LabeledPoint]): GeneralizedLinearModel = {
     val logAlg = new RidgeRegressionWithSGD()
     logAlg.optimizer.setNumIterations(20).setRegParam(1.0).setStepSize(0.00000000001).setUpdater(new SquaredL2Updater)//parameters to optimize gradient descent algorithm
     logAlg.run(trainingData)
   }*/

  def calculateMeanSquaredError(model: GeneralizedLinearModel, testSet: LabeledPoint): Double = {
    /* val valuesAndPreds = testSet.map { point =>
       val prediction = model.predict(point.features)
       (point.label, prediction)
     }
     valuesAndPreds.map { case (v, p) => math.pow((v - p), 2)}.mean()*/
    val prediction = model.predict(Vectors.dense(testSet.features.toArray.slice(0, testSet.features.size - 1)))
    val valuesAndPreds = (testSet.label, prediction)
    println(s"original->${valuesAndPreds._1} predicted->${valuesAndPreds._2}")
    math.pow(valuesAndPreds._1 - valuesAndPreds._2, 2)

  }
  def calculateMeanSquaredErrors(model: GeneralizedLinearModel, testSet: RDD[(Double, Vector)])= {
    /* val valuesAndPreds = testSet.map { point =>
       val prediction = model.predict(point.features)
       (point.label, prediction)
     }

     valuesAndPreds.map { case (v, p) => math.pow((v - p), 2)}.mean()*/
    val pred = testSet.map{feat =>
      val prediction = model.predict(Vectors.dense(feat._2.toArray.slice(0, feat._2.size - 1)))
      (feat._1,prediction)
    }

    pred.foreach(x => println(s"${x._2} was predicted for label ${x._1}"))

  }

  val numFeatures = trainingSet.first()._2.size
  val numCorrections = 10
  val convergenceTol = 1e-4
  val maxNumIterations = 50
  val regParam = 0.1
  val initialWeightsWithIntercept = Vectors.dense(new Array[Double](numFeatures))

  println(numFeatures)
  val (weightsWithIntercept, loss) = LBFGS.runLBFGS(
    trainingSet,
    new LeastSquaresGradient(),
    new SquaredL2Updater(),
    numCorrections,
    convergenceTol,
    maxNumIterations,
    regParam,
    initialWeightsWithIntercept)


  val model = new RidgeRegressionModel(Vectors.dense(weightsWithIntercept.toArray.slice(0, weightsWithIntercept.size - 1)),
    weightsWithIntercept.toArray(weightsWithIntercept.size - 1))


  println("intercept")
  println(model.intercept)
  println("weights---------------------------")
  println(model.weights)
  println("MSE--------------------------------")
  val mse = calculateMeanSquaredError(model, LabeledPoint(trainingSet.first()._1, trainingSet.first()._2))
  println(mse)
  calculateMeanSquaredErrors(model, trainingSet)
  //val predROP = calculateROP(model, data2)
  //predROP.foreach(println(_))
}

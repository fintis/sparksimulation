

import org.apache.spark.mllib.util.MLUtils

import scala.math._

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.optimization._
import org.apache.spark.mllib.regression._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.{Vector, Vectors}

object DrillOptimizer {

  def main(args: Array[String]) {

    if (args.length == 0) {
      println("Please supply data/file")
      System.exit(1)
    }
    val sc = getSparkContext
    val wellData = sc.textFile(args(0)) //read in supplied file

    val data = wellData.map(_.split(" ").map(_.toDouble)).cache() //read one line at a time tokenizing on spaces
    def buildTrainingSet(pcaSet: Boolean) = {
      if (pcaSet) {
        data.map {
          row => //read one array at a time
            val transformedData = BourgoyneYoungModel(row, pcaSet = true) //transform features according to Bourgoyne and Young 1974 drilling model
            // transformedData
            (transformedData.head, MLUtils.appendBias(Vectors.dense(transformedData.tail)))
        }.cache()
      } else {
        data.map {
          row => //read one array at a time
            val transformedData = BourgoyneYoungModel(row) //transform features according to Bourgoyne and Young 1974 drilling model
            // transformedData
            (transformedData.head, MLUtils.appendBias(Vectors.dense(transformedData.tail)))
        }.cache()
      }
    }

    val trainingSetWithPCA = buildTrainingSet(pcaSet = true)
    val trainingSetWithoutPCA = buildTrainingSet(pcaSet = false)
    //println(trainingSetWithoutPCA.first()._2)
    //println(trainingSetWithPCA.first()._2)
    val errorsWithoutPCA = runSimulation(trainingSetWithoutPCA, sc)
    val errorsWithPCA = runSimulation(trainingSetWithPCA, sc)
    sc.stop()
    println(s"mean squared error without PCA:  $errorsWithoutPCA")
    println(s"mean squared error with PCA:  $errorsWithPCA")
  }

  def runSimulation(trainingSet: RDD[(Double, Vector)], sc: SparkContext) = {
    val loops = trainingSet.count().toInt
    val allData = {
      // filter observation in the current iteration from the training set
      for {count <- 0 until loops} yield
        (trainingSet.collect().apply(count), trainingSet.collect().filterNot(x => x == trainingSet.collect().apply(count)))
    }
    val testData = allData.map { data =>
      data._1
    }.toSeq

    val trainData = allData.map { data =>
      data._2.map { d => d}
    }.toSeq.map(sc.parallelize(_))

    var errors = List[Double]()
    for (count <- 0 until trainData.size) {
      val model = produceModel(trainData(count))
      val err = calculateMeanSquaredError(model, testData(count))
      errors ::= err
    }

    val mse = errors.reduce(_ + _) / errors.size

    mse

  }

  def getSparkContext: SparkContext = {
    //get spark context
    //create spark context config
    val conf = new SparkConf().
      setMaster("local").
      setAppName("DrillOptimizer").
      setSparkHome("SPARK_HOME").
      set("spark.executor.memory", "512m").
      set("spark.cleaner.ttl", "3600")
    new SparkContext(conf)

  }

  def BourgoyneYoungModel(params: Array[Double], pcaSet: Boolean = false): Array[Double] = {
    import scala.math.log
    val y = log(params(0))
    val x2 = 10000 - params(1)
    val x3 = pow(params(1), 0.69) * (params(8) - 9)
    val x4 = params(1) * (params(8) - params(7))
    val x5 = log((params(3) - 0.02) / (4 - 0.02))
    val x6 = log(params(4) / 60)
    val x7 = params(5) * (-1)
    val x8 = params(6)
    if (pcaSet) Array(y, x2, x3, x4, x5, x6, x7) else Array(y, x2, x3, x4, x5, x6, x7, x8)

  }


  def calculateMeanSquaredError(model: GeneralizedLinearModel, testSet: (Double, Vector)): Double = {
    val prediction = model.predict(Vectors.dense(testSet._2.toArray.slice(0, testSet._2.size - 1)))
    val valuesAndPreds = (testSet._1, prediction)
    math.pow(valuesAndPreds._1 - valuesAndPreds._2, 2)

  }

  def produceModel(trainingSet: RDD[(Double, Vector)]): GeneralizedLinearModel = {
    val numFeatures = trainingSet.first()._2.size
    val numCorrections = 10
    val convergenceTol = 1e-4
    val maxNumIterations = 50
    val regParam = 0.1
    val initialWeightsWithIntercept = Vectors.dense(new Array[Double](numFeatures))

    println(numFeatures)
    val (weightsWithIntercept, loss) = LBFGS.runLBFGS(
      trainingSet,
      new LeastSquaresGradient(),
      new SquaredL2Updater(),
      numCorrections,
      convergenceTol,
      maxNumIterations,
      regParam,
      initialWeightsWithIntercept)


    new RidgeRegressionModel(Vectors.dense(weightsWithIntercept.toArray.slice(0, weightsWithIntercept.size - 1)),
      weightsWithIntercept.toArray(weightsWithIntercept.size - 1))
  }
}

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.{Matrices, Vectors}
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.optimization._

import scala.math._

object DimensionReduction {

  def main(args: Array[String]) {
    val sc = getSparkContext
    val wellData = sc.textFile(args(0)) //read in supplied file

    val data = {
      wellData.map(_.split(" ").map(_.toDouble)).map { row =>
       // val d = BourgoyneYoungModel(row)
        val batch: Map[String, Double] = Map("rop" -> row(0),
          "depth" -> row(1),
          "wob" -> row(3),
          "rpm" -> row(4),
          "toothWear" -> row(5),
          "reyNum" -> row(6),
          "ecd" -> row(7),
          "ppg" -> row(8))
        Vectors.dense(Array(batch("depth"), batch("wob"), batch("rpm"), batch("toothWear"), batch("reyNum"), batch("ecd"), batch("ppg")))
      }
    }
    println("data: \n" + data.first())
    val matrix = new RowMatrix(data)
    matrix.rows.foreach(println(_))
    // val count = matrix.computeColumnSummaryStatistics().count
    //  val min = matrix.computeColumnSummaryStatistics().min
    //  val max = matrix.computeColumnSummaryStatistics().max
   val mean = matrix.computeColumnSummaryStatistics().mean.toArray
    val sd = stDev(matrix)
    val reg = matrix.rows.map { v =>
      for {i <- 0 until v.toArray.length} yield
        (v.toArray.apply(i) - mean(i)) / sd(i)
    }.map(x => Vectors.dense(x.toArray))

    println(reg.first())
    /* println("count :" + count)
     println("min :" + min)
     println("max :" + max)
     println("mean :" + mean)
     println("var :" + variance)
     println("sd :" + sd)*/
    val regMatrix = new RowMatrix(reg)
    //regMatrix.rows.foreach(println(_))
    val coVarMat = regMatrix.computeCovariance()
    //println("covariance matrix is\n" + coVarMat)
    val dim = regMatrix.numCols().toInt
    val sigma = regMatrix.computeSVD(dim).s
    //val eigenVectors = regMatrix.computeSVD(dim).V

    // sigma.s.toArray.map(e => Math.pow(e, 0.5)).foreach(println(_))
    // sigma.U.rows.foreach(println(_))
    // println(sigma.V)
   val k = {
      for {i <- 1 to dim} yield
        (sigma.toArray.slice(0, i).reduce(_ + _) / sigma.toArray.reduce(_ + _)) >= 0.95
    }.toArray.indexOf(true) + 1
    println("possible K's: " + k)
    val pc = regMatrix.computePrincipalComponents(k)
   // val pc = matrix.computePrincipalComponents(8)
    println("Principal components are:\n"+ pc)

    val projected = regMatrix.multiply(pc)
     println("Principal components when projected are:\n")
    projected.rows.foreach(x => println(x))
    val pcaStDev = Vectors.dense(stDev(projected))
    // println("Standard deviation of PCA:\n" + pcaStDev)
    val pcaCov = sigma.toArray.map { v => v / sigma.toArray.reduce(_ + _) * 100}.toVector
    // println("Covariance of PCA:\n" + pcaCov)
    var pcaCCov = List[Double]()
    var sum: Double = 0
    for (i <- 0 until pcaCov.size) {
      sum = sum + pcaCov(i)
      pcaCCov ::= sum
    }

    // println("Cumulative covariance of    PCA:\n" + pcaCCov.reverse.toVector)
    // println(pc.toArray.toVector)
    //val pcaR = {

    val pcaForTxt = sc.parallelize {
      for (i <- 0 until pc.toArray.length by 8) yield
        pc.toArray.slice(i, i + 8).toVector
    }
    val covForTxt = sc.parallelize {
      for (i <- 0 until coVarMat.toArray.length by 8) yield
        coVarMat.toArray.slice(i, i + 8).toVector
    }

    val unRegProjected = projected.rows.map { p =>
      for {i <- 0 until p.toArray.length} yield
        (mean(i), sd(i))
        //(p.toArray.apply(i)+ mean(i))*sd(i)
    }.map(x => x.toVector)//Vectors.dense(x.toArray))

//unRegProjected.foreach(x=>println(s"${x.}"))

   //println(pcaForTxt.first())
   //println(covForTxt.first())
  //pcaForTxt.saveAsTextFile("file:///c:/tools/msc/data3/pca.txt")
  //covForTxt.saveAsTextFile("file:///c:/tools/msc/data3/cov_mat.txt")
 // projected.rows.saveAsTextFile("file:///c:/tools/msc/data3/proj.txt")
   //unRegProjected.saveAsTextFile("file:///c:/tools/msc/data/unreg_proj.txt")
  //regMatrix.rows.saveAsTextFile("file:///c:/tools/msc/data3/reg_data.txt")
  val stats = sc.parallelize(List(sigma, pcaStDev, pcaCov, pcaCCov.reverse.toVector))
  //stats.foreach(println(_))
 // stats.saveAsTextFile("file:///c:/tools/msc/data3/stats.txt")
  sc.stop()
}

def stDev(mat: RowMatrix) = {
  val va = mat.computeColumnSummaryStatistics().variance
  va.toArray.map(v => Math.pow(v, 0.5))
}

def getSparkContext: SparkContext = {
  //get spark context
  //create spark context config
  val conf = new SparkConf().
    setMaster("local").
    setAppName("DrillOptimizer").
    setSparkHome("SPARK_HOME").
    set("spark.executor.memory", "512m").
    set("spark.cleaner.ttl", "3600")
  new SparkContext(conf)

}
  def BourgoyneYoungModel(params: Array[Double]): Array[Double] = {
    import scala.math.log
   // val y = log(params(0)) omit the labels for PCA
    val x2 = 10000 - params(1)
    val x3 = pow(params(1), 0.69) * (params(8) - 9)
    val x4 = params(1) * (params(8) - params(7))
    val x5 = log((params(3) - 0.02) / (4 - 0.02))
    val x6 = log(params(4) / 60)
    val x7 = params(5) * (-1)
    val x8 = params(6)
    Array( x2, x3, x4, x5, x6, x7, x8)
    //param(2) is not used bit number
  }
}

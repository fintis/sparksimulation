import org.apache.spark.mllib.clustering.{KMeansModel, KMeans}
import org.apache.spark.mllib.feature.{StandardScaler, Normalizer}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.{Vectors, Vector}

import scala.math._

object KMeansClustering extends App {

  val sc = getSparkContext
  val wellData = sc.textFile(args(0)) //read in supplied file

  val data = wellData.map(_.split(" ").map(_.toDouble)).cache()
  //read one line at a time tokenizing on spaces
  val trainingSet = data.map {
    row => //read one array at a time
      val batch: Map[String, Double] = Map("rop" -> row(0),
        "depth" -> row(1),
        "wob" -> row(3),
        "rpm" -> row(4),
        "toothWear" -> row(5),
        "reyNum" -> row(6),
        "ecd" -> row(7),
        "ppg" -> row(8))
      Vectors.dense(Array(batch("depth"), batch("wob"), batch("rpm"), batch("toothWear"), batch("reyNum"), batch("ecd"), batch("ppg")))
    /* val transformedData = BourgoyneYoungModel(row) //transform features according to Bourgoyne and Young 1974 drilling model
 Vectors.dense(transformedData)
 //LabeledPoint(transformedData.head, transformedData.tail) //prepare predictor and output for regression
 // (transformedData.head, transformedData.tail)*/
  }.cache()

  def getSparkContext: SparkContext = {
    //get spark context
    //create spark context config
    val conf = new SparkConf().
      setMaster("local").
      setAppName("DrillOptimizer").
      setSparkHome("SPARK_HOME").
      set("spark.executor.memory", "512m").
      set("spark.cleaner.ttl", "3600")
    new SparkContext(conf)

  }

  /*def BourgoyneYoungModel(params: Array[Double]): Array[Double] = {
    import scala.math.log
    val y = log(params(0))
    val x2 = 8000 - params(1)
    val x3 = pow(params(1), 0.69) * (params(8) - 9)
    val x4 = params(1) * (params(8) - params(7))
    val x5 = log((params(3) - 0.02) / (4 - 0.02))
    val x6 = log(params(4) / 60)
    val x7 = params(5) * (-1)
    val x8 = params(6)
    Array(y, x2, x3, x4, x5, x6, x7, x8)
    //param(2) is not used bit number
  }*/

  val normalizer = new Normalizer(p = Double.PositiveInfinity)
  val numClusters = 31
  val numIterations = 10
  var WSSSE = List[(Int, Double)]()
  var vectorsAndClusterIdx = List[RDD[(Int, Vector, Int)]]()
  var clusterCentres = List[(Int, Array[Vector])]()
  for {i <- 2 until numClusters} yield {
   // val scalar = new StandardScaler(withMean = true, withStd = true).fit(trainingSet)
    val normalizedTraining = trainingSet.map { raw =>
      //scalar.transform(raw)
      normalizer.transform(raw)
    }
    //val cluster = KMeans.train(trainingSet, i, numIterations, 2, KMeans.K_MEANS_PARALLEL)//raw
    val cluster = KMeans.train(normalizedTraining, i, numIterations, 5, KMeans.K_MEANS_PARALLEL) //normalized
    //WSSSE ::=(i, cluster.computeCost(trainingSet))//raw
    WSSSE ::=(i, cluster.computeCost(normalizedTraining)) //normalized
    val vAc = normalizedTraining.map { point: Vector =>
        val prediction: Int = cluster.predict(point)
        // val prediction = cluster.predict(point)
        (i, point, prediction)
      }
    clusterCentres ::=(i, cluster.clusterCenters)
    vectorsAndClusterIdx ::= vAc
  }
  // Evaluate clustering by computing Within Set Sum of Squared Errors


  vectorsAndClusterIdx.map { x =>
    //x.saveAsTextFile(s"file:///c:/tools/data/kmeans_cluster_S${x.first()._1}")
    x.foreach(y => println(s"numCluster: ${y._1} of vectors: ${y._2} belong to cluster: ${y._3}"))
  }
  //sc.parallelize(WSSSE).saveAsTextFile("file:///c:/tools/data/kmeans_cluster_cost_S")
  clusterCentres.map{ct =>
    //sc.parallelize(ct._2).saveAsTextFile(s"file:///c:/tools/data/kmeans_cluster_centres_N${ct._1}")

  }

  WSSSE.foreach(x => println(s"numCluster: ${x._1} cost: ${x._2}"))
  clusterCentres.foreach(x => println(s"numCluster: ${x._1} centre: ${x._2}"))
  sc.stop()
  //vectorsAndClusterIdx.foreach(x =>println(s"vectors: ${x._1} belong to cluster: ${x._2}"))
  //println(s"Within Set Sum of Squared Errors = $WSSSE")
  // println(s"cluster centres :${clusters.clusterCenters.toVector}")
}
